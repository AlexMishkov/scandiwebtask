<?php
namespace Product;


class Furniture extends Product {
    // Here we add the special attributes for this class that are different from other Products
    public $attrUnit = 'HxWxL';
    public $typeSlug = 'furniture';

}
?>
