<?php

namespace Product\Database;


use mysqli;

use \PDO;

// This is the class for connecting with the Database
class DB {
    public static $pdo = null;
    

    public static function connect(){
        

        if(is_null(self::$pdo)){
            try{
                self::$pdo = new \PDO("mysql:dbname=" . DBNAME . ";host=" . HOST, MYACCOUNT, MYPASSWORD);

            }catch(\PDOException $e){
                die("Can not connect");
            }
        }
    }
}



?>