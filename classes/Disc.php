<?php
namespace Product;


class Disc extends Product{
    // Here we add the special attributes for this class that are different from other Products
    public $attrUnit = 'MB';
    public $typeSlug = 'disc';

}
?>
