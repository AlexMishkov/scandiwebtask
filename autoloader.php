<?php
$autoload = ['interfaces/ProductInterface.php','classes/Product.php','classes/Disc.php','classes/Book.php','classes/Furniture.php','classes/DB.php','constants/constants.php'];
foreach($autoload as $class){
    require_once $class;
}