<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

    <title>Junior Dev Test</title>
</head>

<body>
    <div class="container">
        <div class="row mt-5">
            <div class="col-12 d-flex border-bottom pb-3">
                <h2>Product List</h2>
                <div class="d-flex ms-auto">
                    <button class="btn btn-success me-3 save">
                        Save</button>
                    <a href="index.php" class="btn btn-warning me-3">
                        Cancel</a>
                </div>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-6">
                <form action="controllers/productController.php" method="POST" id="product_form">
                    <div class="col-auto">
                        <label for="sku" class="col-form-label">SKU</label>
                        <p class="text-danger sku-validation"></p>
                        <input type="text" name="sku" class="form-control" id="sku">
                    </div>
                    <div class="col-auto mt-3">
                        <label for="name" class="col-form-label">Name</label>
                        <p class="text-danger name-validation"></p>
                        <input type="text" name="name" class="form-control" id="name">
                    </div>
                    <div class="col-auto mt-3">
                        <label for="price" class="col-form-label">Price ($)</label>
                        <p class="text-danger price-validation"></p>
                        <input type="number" name="price" class="form-control" id="price">
                    </div>

                    <div class="col-auto mt-3">
                        <label for="productType" class="col-form-label">Prodyct Type</label>
                        <p class="text-danger type-validation"></p>
                        <select id="productType" name="product_type" class="form-select">
                            <option value="disc" id="DVD">DVD</option>
                            <option value="furniture" id="Furniture">Furniture</option>
                            <option value="book" id="Book">Book</option>
                        </select>
                    </div>

                    <div class="col-auto mt-5" id="specialAttribute">
                        <div class="col-auto">
                            <label for="size" class="col-form-label">Size (MB)</label>
                            <p class="text-danger size-validation validation"></p>
                            <input type="number" name="size" class="form-control" id="size">
                        </div>
                        <h6 class="my-2">Please provide Disc size in MB</h6>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Footer -->
    <footer class="text-center border-top mt-4">
        <h4>Scandiweb Test assignment</h4>
    </footer>

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"
        integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.min.js"
        integrity="sha384-Atwg2Pkwv9vp0ygtn1JAojH0nYbwNJLPhwyoVbhoPwBhjQPR5VtM2+xf0Uwh9KtT" crossorigin="anonymous">
    </script>


    <!-- Jquery -->
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

    <!-- Custom Scripts -->
    <script src="assets/JS/special_attr.js"></script>
    <script src="assets/JS/form_validation.js"></script>
</body>

</html>