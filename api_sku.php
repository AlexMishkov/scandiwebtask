<?php
require_once 'autoloader.php';
use Product\Database\DB;

if($_SERVER["REQUEST_METHOD"] == "POST"){
DB::connect();
$sql = "SELECT sku FROM products WHERE 1";
$stmt = DB::$pdo->query($sql);
$allSku = [];

while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
    $allSku[] = $row;
}

echo json_encode($allSku, JSON_PRETTY_PRINT);
}
