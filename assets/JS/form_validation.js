$(document).ready(function(){
    // Here starts the validation for the Product Form
    $('.save').on('click', function(){
        let error = false;
        if(jQuery.trim($('#sku').val()).length == 0){
            $('.sku-validation').text('You must enter a SKU code');
            error = true;
        }else{
            $('.sku-validation').empty();
            
        }

        // This is the ajax request to check database for existing SKU codes
        let ajaxRequest = $.ajax({
            dataType: "json",
            url: "api_sku.php",
            method: 'POST',
            success: function(data) {
                let skuVal = $('#sku').val();
                $.each(data, function(key,value){
                    if(value.sku == skuVal){
                        error = true
                        $('.sku-validation').text('SKU already exists');
                        return error;
                    }
                })
            }
        });

        // Validating other fields after Ajax request for SKU code
        $.when(ajaxRequest).done(function(){
            if(jQuery.trim($('#name').val()).length == 0){
                $('.name-validation').text('You must enter a product name');
                error = true;
            }else{
                $('.name-validation').empty();
            }
    
            if(jQuery.trim($('#price').val()).length == 0){
                $('.price-validation').text('You must enter a product Price in USD$');
                error = true;
            }else{
                $('.price-validation').empty();
            }
            
            if ($('#productType').val() == 'disc'){
                if(jQuery.trim($('#size').val()).length == 0){
                    $('.size-validation').text('You must enter size in MB');
                    error = true;
                }else{
                    $('.size-validation').empty();
                }
            }else if($('#productType').val() == 'book'){
                if(jQuery.trim($('#weight').val()).length == 0){
                    $('.weight-validation').text('You must enter weight in KG');
                    error = true;
                }else{
                    $('.weight-validation').empty();
                }
            }else if ($('#productType').val() == 'furniture'){
                if(jQuery.trim($('#height').val()).length == 0){
                    $('.height-validation').text('You must enter height in M');
                    error = true;
                }else{
                    $('.height-validation').empty();
                }
                if(jQuery.trim($('#width').val()).length == 0){
                    $('.width-validation').text('You must enter width in M');
                    error = true;
                }else{
                    $('.width-validation').empty();
                }
                if(jQuery.trim($('#length').val()).length == 0){
                    $('.length-validation').text('You must enter length in M');
                    error = true;
                }else{
                    $('.length-validation').empty();
                }
            }
            console.log(error)
            if(error === false){

                $('#product_form').submit()
            }
        })
        
    })
})