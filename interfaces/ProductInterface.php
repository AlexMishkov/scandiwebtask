<?php
namespace Product\Interfaces;

interface ProductInterface {
    public function insert();
    public function getTypeId($typeSlug);
    public function getAttributeId($attributeUnit);
    public static function deleteProduct($skuArray);
}






?>