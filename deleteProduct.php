<?php
// $autoload = ['interfaces/Insert.php', 'classes/Product.php','classes/Disc.php','classes/Book.php','classes/Furniture.php','classes/DB.php','constants/constants.php'];
// foreach($autoload as $class){
//     require_once $class;
// }
require_once 'autoloader.php';

use Product\Database\DB;
use Product\Product;

if ($_POST["sku"]) {
    Product::deleteProduct($_POST["sku"]);
}

?>