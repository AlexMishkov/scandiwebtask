<?php
// $autoload = ['../interfaces/Insert.php', '../classes/Product.php','../classes/Disc.php','../classes/Book.php','../classes/Furniture.php','../classes/DB.php','../constants/constants.php'];
// foreach($autoload as $class){
//     require_once $class;
// }
require_once '../autoloader.php';


use Product\Product;
use Product\Disc;
use Product\Book;
use Product\Furniture;


if($_SERVER["REQUEST_METHOD"] == "POST"){
    // Main ADD logic
    
    if(isset($_POST['size'])){
        $attribute_value = $_POST['size'];
    }

    if(isset($_POST['weight'])){
        $attribute_value = $_POST['weight'];
    }

    if(isset($_POST['height']) && isset($_POST['width']) && isset($_POST['length'])){
        $attribute_value = $_POST['height'] . 'x' . $_POST['width'] . 'x' . $_POST['length'];
    }

    $sku = $_POST['sku'];
   
    $name = $_POST['name'];
    $price = $_POST['price'];    
    $productType = $_POST['product_type'];

    if($productType == 'disc'){
        $disc = new Disc($sku, $name, $price, $attribute_value);
        $disc->insert();
    }else if($productType == 'book'){
        $book = new Book($sku, $name, $price, $attribute_value);
        $book->insert();
    }else if($productType == 'furniture'){
        $furniture = new Furniture($sku, $name, $price, $attribute_value);
        $furniture->insert();
    }
    header("Location: " . HOMEPAGE);
}




?>